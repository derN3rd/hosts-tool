#!/bin/bash

if [ $UID != 0 ]; then
        echo "Run me as sudo/root!"
        exit 1
fi

if [ ! -d "/etc/hosts.d" ]; then
        echo "Run \"hosts-tool setup\" to start using the tool"
        exit 1
fi

if [ $# -lt 1 ]; then
        cp -f /etc/hosts /etc/hosts.bak
        cd /etc/hosts.d/
        for file in `ls | grep "^[0-9]"`
        do
                echo "# $file" >> /etc/hosts.tmp
                cat $file >> /etc/hosts.tmp
        done
        mv -f /etc/hosts.tmp /etc/hosts
else
        if [ $1 = "restore" ]; then
                mv -f /etc/hosts.bak /etc/hosts
        elif [ $1 = "setup" ]; then
                mkdir /etc/hosts.d/
                cp /etc/hosts /etc/hosts.d/00-default
        else
                echo "Possible arguments:"
                echo "                  - no argument will merge all your hosts.d files to /etc/hosts"
                echo "          restore - restores the last version of /etc/hosts file"
                echo "          setup   - run this on first run. (creates /etc/hosts.d/ folder and copies /etc/hosts to it)"
                exit 2;
        fi
fi