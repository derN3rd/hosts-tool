# hosts-tool

Hosts-tool is a little script that will keep your hosts file clean.
With hosts-tool you don't edit the /ect/hosts file by yourself. You create multiple files at /etc/hosts.d/ and then they will be merged to /etc/hosts

Usage
---
 - `hosts-tool` - Does the magic, merges all your files to one hosts file
 - `hosts-tool restore` - Restores the last hosts-tool change
 - `hosts-tool setup` - Creates the /etc/hosts.d/ folder and copies /etc/hosts to /etc/hosts.d/00-default

Installation
---
Run the following steps to install hosts-tool globally:
1. `wget https://gitlab.com/derN3rd/hosts-tool/raw/master/hosts-tool.sh`
2. `sudo cp ./hosts-tool.sh /usr/bin/hosts-tool`
3. `sudo chmod +x /usr/bin/hosts-tool`
4. You are ready to use `hosts-tool` as a command


How to stop a hosts file from being included?
---
The merge looks up the file name. If it begins with 0-9 then the file will be merged, if not it won't be merged.
Simply add a `_` at the beginning.
Example. The following files:
```
00-default
10-work-stuff
20-private-stuff
_21-old-private-stuff
```
will result in only the first 3 merged.